const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

function allAvailableItems(items) {
    const allItems = items.map(item => {

        return item.name
    })
    return allItems

}
// console.log(allAvailableItems(items))

function allItemsContainingVitaminC(items) {
    const itemContainingVitaminC = items.filter(item => {
        return item.contains.includes('Vitamin C')
    })
    return itemContainingVitaminC
}
// console.log(allItemsContainingVitaminC(items))


function allItemsContainingVitaminA(items) {
    const itemContainingVitaminA = items.filter(item => {
        return item.contains.includes('Vitamin A')
    })
    return itemContainingVitaminA
}
// console.log(allItemsContainingVitaminA(items))


function allItemsBasedOnVitamins(items) {
    const allVitaminsAndNames = items.reduce((result, item) => {
        let vitamin = item.contains.split(',')

        if (vitamin.length > 1) {
            result.push([vitamin[0].trim(), item.name])
            result.push([vitamin[1].trim(), item.name])
        }
        else {
          
            result.push([vitamin[0].trim(), item.name])
        }
        return result
    }, [])
        .reduce((result, vitamin) => {
            if (result[vitamin[0]]) {
                result[vitamin[0]].push(vitamin[1])
            } else {
                result[vitamin[0]] = []
                result[vitamin[0]].push(vitamin[1])
            }
            return result
        }, {})

    return allVitaminsAndNames
}

console.log(allItemsBasedOnVitamins(items))

function sortItemsBasedOnVitamins(items) {
    let sortedItems = items.sort((item1, item2) => {
        let numberOfVitaminsOfItems1 = item1.contains.split(',').length
        let numberOfVitaminsOfItems2 = item2.contains.split(',').length

        return numberOfVitaminsOfItems1 - numberOfVitaminsOfItems2
    })
    return sortedItems
}
// console.log(sortItemsBasedOnVitamins(items))